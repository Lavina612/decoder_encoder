﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Decoder_Encoder
{
	public partial class MainWindow : Window
	{
		AuxiliaryMethods am = new AuxiliaryMethods();
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Button_Decode(object sender, RoutedEventArgs rea)
		{
			RadioButton rbSelected = null;
			int decKey = -1;
			bool decDirection = true;
			int check = am.CheckPath(decoderPathFrom, "Для дешифратора не указан путь к файлу",
				"Заполнены не все поля", "По ТЗ был формат txt. Так что ничего не знаю, у меня лапки ^.^",
				"Придётся менять формат", out string pathFrom);
			if (check != 0)
				return;

			List<RadioButton> radioButtons = decoderGrid.Children.OfType<RadioButton>().ToList();
			rbSelected = radioButtons.Where(r => r.GroupName == "DecoderMethod" && (bool)r.IsChecked).Single();
			if (rbSelected.Content.ToString() == "по определённому ключу")
			{
				int keyAndDirection = am.GetOneKeyAndDirection(decoderDirection, decoderKey,
					"Ключ для дешифратора не является числом", "Некорреткно заполнены поля",
					"Ключ для дешифратора не может быть отрицательным числом или нулём",
					"Некорреткно заполнены поля", out decKey, out decDirection);
				if (keyAndDirection != 0)
					return;
			}

			string result = "";
			int read = am.ReadFromFile(pathFrom, out string textFromFile);
			if (read != 0)
				return;

			Decoder decoder = new Decoder();
			switch (rbSelected.Content.ToString())
			{
				case "частотный анализ":
					result = decoder.FrequencyAnalysis(textFromFile);
					break;
				case "по всем ключам":
					result = decoder.DecodeByAllKeys(textFromFile);
					break;
				case "по определённому ключу":
					result = decoder.DecodeByOneKey(textFromFile, decKey, decDirection);
					break;
			}
			decoderResultText.Text = result;
		}


		private void Button_DecoderSave(object sender, RoutedEventArgs rea)
		{
			int check = am.CheckPath(decoderPathTo, "Не указан путь к файлу для сохранения",
				"Поле не заполнено", "По ТЗ был формат txt. Так что ничего не знаю, у меня лапки ^.^",
				"Придётся менять формат", out string pathTo);
			if (check != 0)
				return;
			int write = am.WriteToFile(pathTo, decoderResultText);
			if (write == 0)
				MessageBox.Show("Дешифрованный текст сохранён в файл " + pathTo, "Успешное сохранение",
					MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void Button_Encode(object sender, RoutedEventArgs rea)
		{
			string pathFrom = "";
			string textSource = "";
			int encKey = -1;
			bool encDirection = true;
			List<RadioButton> radioButtons = encoderGrid.Children.OfType<RadioButton>().ToList();
			RadioButton rbSelected = radioButtons.Where(r => r.GroupName == "encoderPathFrom" && (bool)r.IsChecked).Single();
			if (rbSelected.Content.ToString() == "Из файла:")
			{
				int check = am.CheckPath(encoderPathFrom, "Для шифратора не указан путь к файлу",
					"Заполнены не все поля", "По ТЗ был формат txt. Так что ничего не знаю, у меня лапки ^.^",
					"Придётся менять формат", out pathFrom);
				if (check != 0)
					return;
			}
			else
			{
				int read = am.ReadFromConsole(encoderTextFrom, "Отсутствует текст в консоли для шифратора",
					"Заполнены не все поля", out textSource);
				if (read != 0)
					return;
			}
			rbSelected = radioButtons.Where(r => r.GroupName == "encoderMethod" && (bool)r.IsChecked).Single();
			if (rbSelected.Content.ToString() == "по определённому ключу")
			{
				int keyAndDirection = am.GetOneKeyAndDirection(encoderDirection, encoderKey,
					"Ключ для шифратора не является числом", "Некорреткно заполнены поля",
					"Ключ для шифратора не может быть отрицательным числом или нулём", "Некорреткно заполнены поля",
					out encKey, out encDirection);
			}

			string result = "";
			if (pathFrom != "")
			{
				int read = am.ReadFromFile(pathFrom, out textSource);
				if (read != 0)
					return;
			}
			Decoder decoder = new Decoder();
			switch (rbSelected.Content.ToString())
			{
				case "по всем ключам":
					result = decoder.DecodeByAllKeys(textSource);
					break;
				case "по определённому ключу":
					result = decoder.DecodeByOneKey(textSource, encKey, encDirection);
					break;
			}
			
			encoderResultText.Text = result;
		}

		private void Button_EncoderSave(object sender, RoutedEventArgs rea)
		{
			int check = am.CheckPath(encoderPathTo, "Не указан путь к файлу для сохранения",
				"Поле не заполнено", "По ТЗ был формат txt. Так что ничего не знаю, у меня лапки ^.^",
				"Придётся менять формат", out string pathTo);
			if (check != 0)
				return;
			int write = am.WriteToFile(pathTo, encoderResultText);
			if (write == 0)
				MessageBox.Show("Зашифрованный текст сохранён в файл " + pathTo, "Успешное сохранение", 
					MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void RadioButton_CheckedDecoderDisableKey(object sender, RoutedEventArgs e)
		{
			decoderKeyGrid.IsEnabled = false;
		}

		private void RadioButton_CheckedDecoderEnableKey(object sender, RoutedEventArgs e)
		{
			decoderKeyGrid.IsEnabled = true;
		}

		private void RadioButton_EncoderDisableConsole(object sender, RoutedEventArgs e)
		{
			encoderTextFrom.IsEnabled = false;
			encoderPathFrom.IsEnabled = true;
		}

		private void RadioButton_EncoderDisablePathFrom(object sender, RoutedEventArgs e)
		{
			encoderPathFrom.IsEnabled = false;
			encoderTextFrom.IsEnabled = true;
		}

		private void RadioButton_CheckedEncoderDisableKey(object sender, RoutedEventArgs e)
		{
			encoderKeyGrid.IsEnabled = false;
		}

		private void RadioButton_CheckedEncoderEnableKey(object sender, RoutedEventArgs e)
		{
			encoderKeyGrid.IsEnabled = true;
		}

		private void Button_DecoderInformation(object sender, RoutedEventArgs e)
		{
			MessageBox.Show("Дешифратор - специальная программа, которая позволяет расшифровать исходный текст, " +
				"закодированный шифром Цезаря.\n\nЧтобы расшифровать текст, Вам нужно указать путь к файлу, в котором " +
				"находится нужный текст. Вложенность директорий в пути указывается через / или \\\\. Если путь начинается " +
				"не с родительской директории, то программа автоматически произведёт поиск файла именно в ней. " +
				"Внимательно проверяйте корректность указанного пути!\n\nЗатем следует выбрать один из перечисленных " +
				"способов дешифровки: частотный анализ, по всем ключам или по определённому ключу.\n\nЧастотный анализ.\n" +
				"Считается, что в русском языке наиболее частая буква это \"о\". Частотный анализ пытается расшифровать " +
				"текст, слепо доверясь этому утверждению. Существует большая вероятность, что в результате частотного " +
				"анализа текст будет расшифрован неправильно (например, если текст очень маленький или чаще всего в нём " +
				"встречается не буква \"о\"). Таким образом, Вы можете выбрать метод частотного анализа только на свой страх " +
				"и риск. Если же Вы сомневаетесь, лучше воспользоваться способом расшифровки по всем ключам.\n\nМетод " +
				"расшифровки по всем ключам предполагает перебор всех 33 вариантов, из которых лишь один окажется верным. " +
				"Все варианты Вам будут представлены.\n\nМетод расшифровки по определённому ключу предполагает, что Вы " +
				"знаете, с каким именно ключом был зашифрован текст. Вы вводите ключ (положительное число до 100), а затем " +
				"выбираете направление (влево/вправо), куда будет дешифрован текст.\nПримечание: если будет указан " +
				"отрицательный ключ и направление \"вправо\", то такие параметры будут автоматически заменены на " +
				"положительный ключ с тем же значением и направление \"влево\".\n\nПосле того, как все необходимые поля " +
				"будут заполнены, нажминте кнопку \"Дешифровать\" и в большом текстовом поле появится расшифрованный текст " +
				"(или несколько, если был выбран способ дешифровки по всем ключам).\n\nЕсли Вам потребуется сохранить " +
				"получившийся результат, то необходимо указать нужный путь к файлу (он указывается по таким же правилам, " +
				"как и путь к файлу, в котором хранился зашифрованный текст) в нижнем поле для ввода, а затем нажать " +
				"\"Сохранить\".", "Дешифратор", MessageBoxButton.OK);
		}

		private void Button_EncoderInformation(object sender, RoutedEventArgs e)
		{
			MessageBox.Show("Шифратор - специальная программа, которая позволяет зашифровать исходный текст " +
				"с помощью шифра Цезаря.\n\nЧтобы зашифровать текст, Вам нужно либо написать исходный текст в консоль, " +
				"либо указать путь к файлу, в котором он находится. Вложенность директорий в пути указывается " +
				"через / или \\\\. Если путь начинается не с родительской директории, то программа автоматически " +
				"произведёт поиск файла именно в ней. Внимательно проверяйте корректность указанного пути!\n\nЗатем " +
				"следует выбрать один из перечисленных способов зашифровки: по всем ключам или по определённому " +
				"ключу.\n\nМетод зашифровки по всем ключам предполагает шифрацию текста с использованием всех 33 " +
				"ключей. Все получившиеся варианты Вам будут представлены.\n\nМетод зашифровки по определённому ключу " +
				"предполагает шифровку с заранее заданным одним ключом и направлением. Вы вводите ключ (положительное " +
				"число до 100), а затем выбираете направление (влево/вправо), куда будет дешифрован текст.\nПримечание: " +
				"если будет указан отрицательный ключ и направление \"вправо\", то такие параметры будут автоматически " +
				"заменены на положительный ключ с тем же значением и направление \"влево\".\n\nПосле того, как все " +
				"необходимые поля будут заполнены, нажминте кнопку \"Зашифровать\" и в большом текстовом поле появится " +
				"зашифрованный текст (или несколько, если был выбран способ зашифровки по всем ключам).\n\nЕсли Вам " +
				"потребуется сохранить получившийся результат, то необходимо указать нужный путь к файлу (он " +
				"указывается по таким же правилам, как и путь к файлу, в котором хранился исходный текст) в нижнем " +
				"поле для ввода, а затем нажать \"Сохранить\".", "Шифратор", MessageBoxButton.OK);
		}
	}
}
