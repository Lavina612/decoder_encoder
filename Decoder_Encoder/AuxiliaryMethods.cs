﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Decoder_Encoder
{
	public class AuxiliaryMethods
	{
		public int CheckPath(TextBox tb, string emptyPathMessage, string emptyPathTitle, string notTxtMessage,
			string notTxtTtile, out string pathFrom)
		{
			pathFrom = tb.Text.Trim();
			if (pathFrom == "")
			{
				MessageBox.Show(emptyPathMessage, emptyPathTitle, MessageBoxButton.OK, MessageBoxImage.Error);
				return -1;
			}
			if (pathFrom.Split('.').Length != 1 && pathFrom.Split('.').Last() != "txt")
			{
				MessageBox.Show(notTxtMessage, notTxtTtile, MessageBoxButton.OK, MessageBoxImage.Error);
				return -2;
			}
			pathFrom = (pathFrom.ToUpper().StartsWith("C:/") || pathFrom.StartsWith("/")) ? pathFrom : "/" + pathFrom;
			return 0;
		}

		public int ReadFromFile(string pathFrom, out string textFromFile)
		{
			textFromFile = "";
			try
			{
				StreamReader sr = new StreamReader(pathFrom, Encoding.GetEncoding(1251));
				textFromFile = sr.ReadToEnd();
				sr.Close();
			}
			catch (FileNotFoundException e)
			{
				MessageBox.Show("Не удаётся найти файл " + pathFrom, "Неверный путь файла", MessageBoxButton.OK,
					MessageBoxImage.Error);
				return -1;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.GetType().Name + ": " + e.Message);
				return -2;
			}
			return 0;
		}

		public int ReadFromConsole(TextBox tb, string emptyTextMessage, string emptyTextTitle, out string textSource)
		{
			textSource = tb.Text.Trim();
			if (textSource == "")
			{
				MessageBox.Show(emptyTextMessage, emptyTextTitle, MessageBoxButton.OK, MessageBoxImage.Error);
				return -1;
			}
			return 0;
		}

		public int WriteToFile(string pathTo, TextBox tb)
		{
			try
			{
				FileStream fs = new FileStream(pathTo, FileMode.OpenOrCreate);
				StreamWriter sr = new StreamWriter(fs, Encoding.GetEncoding(1251));
				foreach (string str in tb.Text.Split('\n'))
					sr.WriteLine(str);
				sr.Close();
			}
			catch (FileNotFoundException e)
			{
				MessageBox.Show("Не удаётся найти файл " + pathTo, "Неверный путь файла", MessageBoxButton.OK,
					MessageBoxImage.Error);
				return -1;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.GetType().Name + ": " + e.Message);
				return -2;
			}
			return 0;
		}

		public int GetOneKeyAndDirection(ComboBox cb, TextBox tb, string notNubmerMessage, string notNumberTitle, string negativeNumberMessage,
			string negativeNumberTitle, out int decKey, out bool decDirection)
		{
			decDirection = (((TextBlock)((ComboBoxItem)cb.SelectedItem).Content).Text.ToString()
				== "вправо") ? true : false;
			if (!int.TryParse(tb.Text.Trim(), out decKey))
			{
				MessageBox.Show(notNubmerMessage, notNumberTitle, MessageBoxButton.OK, MessageBoxImage.Error);
				return -1;
			}
			if (decKey < 1 || decKey > 99)
			{
				MessageBox.Show(negativeNumberMessage, negativeNumberTitle, MessageBoxButton.OK, MessageBoxImage.Error);
				return -2;
			}
			return 0;
		}
	}
}
