﻿namespace Decoder_Encoder
{
	public class Decoder
	{
		public string FrequencyAnalysis(string text)
		{
			string result = "";
			char mostFreq = '\0';
			int maxFreq = -1;
			int[] frequencies = new int[33];
			foreach (char ch in text)
			{
				if ((char.ToLower(ch) >= 'а' && char.ToLower(ch) <= 'я') || char.ToLower(ch) == 'ё')
				{
					if (char.ToLower(ch) == 'ё')
						frequencies[32]++;
					else
						frequencies[char.ToLower(ch) - 'а']++;
				}
			}
			for (int i = 0; i < frequencies.Length - 1; i++)
			{
				if (frequencies[i] > maxFreq)
				{
					maxFreq = frequencies[i];
					mostFreq = (char)(i + 'а');
				}
			}
			if (frequencies[32] > maxFreq)
			{
				maxFreq = frequencies[32];
				mostFreq = 'ё';
			}
			int key = 0;
			if (mostFreq != 'ё')
				key = 'о' - mostFreq;
			else if (mostFreq == 'ё')
				key = mostFreq - 'о' - 21; //я честно скажу, я подобрала это число 21 рандомным образом
			result += "Получившийся ключ: " + key + "\n\n";
			result += DecodeByOneKey(text, key, true);
			return result;
		}

		public string DecodeByOneKey(string text, int key, bool direction)
		{
			if (!direction) key = -key;
			string result = "";
			int index = -1;
			foreach (char ch in text)
			{
				if (char.ToLower(ch) >= 'а' && char.ToLower(ch) <= 'я' || char.ToLower(ch) == 'ё')
				{
					if (char.ToLower(ch) <= 'е')
						index = char.ToLower(ch) - 'а';
					else if (char.ToLower(ch) == 'ё')
						index = 6;
					else index = char.ToLower(ch) - 'а' + 1;

					index += key;
					index = index % 33;
					index = (index >= 0) ? index : 33 + index;
					if (index != 6)
					{
						if (index > 6)
							index--;
						if (char.IsUpper(ch))
							result += (char)(index + 'А');
						else result += (char)(index + 'а');
					}
					else
					{
						if (char.IsUpper(ch))
							result += 'Ё';
						else result += 'ё';
					}

				}
				else result += ch;
			}
			return result;
		}

		public string DecodeByAllKeys(string text)
		{
			string result = "";
			for(int i = 1; i < 33; i++)
			{
				result += "Ключ: " + i + "\n\n";
				result += DecodeByOneKey(text, i, true);
				result += "\n\n\n";
			}
			result += "Ключ: " + 33 + "\n\n";
			result += DecodeByOneKey(text, 33, true);
			return result;
		}
	}
}
