Decoder_Encoder
---------------

������ solution �������� ��� �������: Decoder_Encoder � ������������ �������� ��� ������ ���������� � Decoder_Encoder_Tests ��� ������������ ��� ����������������.

Decoder_Encoder project
-----------------------
���� ������ �������� � ���� ��� ����������� ������ ��� ������ ����������: MainWindow.xaml.cs ��� �������������� ������������ � ���������� (������� ������, �������������� � �.�.), Decoder.cs � AuxiliaryMethods.cs.

Decoder.cs
----------
���� ����� �������� � ���� ��� ����������� �������, ������� ������ ������ ������� ���������� ����������� ������: 
1) string DecodeByOneKey(string text, int key, bool direction) ��������� �������� �����, ���� � ����������� � ���������� �����, ���������� ����������� ��������� ������ � ������� ����� ������ �� ��������� ����� � �����������.
2) string DecodeByAllKeys(string text) ��������� �������� ����� � ���������� 33 ������� �������������� ������ ��� � ������� ����� ������ �� ����� ������ ������ (33). ��� �������� ������� � ���� ������������ �����.
3) string FrequencyAnalysis(string text) ��������� �������� ����� � ���������� ������������� ����� � ������� ����� ������ � �������������� ���������� �������, ������� ����������, ��� �������� ������ ����� � ������� ����� ��� "�".

AuxiliaryMethods.cd
-------------------
���� ����� �������� � ���� �������������� ������ ��� �������������� ������������ � �����������:
1) int CheckPath(TextBox tb, string emptyPathMessage, string emptyPathTitle, string notTxtMessage, string notTxtTtile, out string pathFrom) ��������� ���� � �����, ������� � TextBox, �� ������������� � ���������� 0 � ������ ������.
2) int ReadFromFile(string pathFrom, out string textFromFile) ������ �� ����� ���� ����� � ���������� 0 � ������ ������.
3) int ReadFromConsole(TextBox tb, string emptyTextMessage, string emptyTextTitle, out string textSource) ��������� ���� ����� � ������� (TextBox) � ���������� 0 � ������ ������.
4) int WriteToFile(string pathTo, TextBox tb) ���������� � ���� ����� �� TextBox � ���������� 0 � ������ ������.
5) int GetOneKeyAndDirection(ComboBox cb, TextBox tb, string notNubmerMessage, string notNumberTitle, string negativeNumberMessage, string negativeNumberTitle, out int decKey, out bool decDirection) �������� ������� �� ���������� �������� ������������� ���� � ����������� � ���������� 0 � ������ ������.

Decoder_Encoder_Tests project
-----------------------------
���� ������ �������� ������ ��� ������������ ��������� ����������� ����������

DecoderTest.cs
--------------
�������� � ���� �����, ������� ��������� ������������ ������ ��� ������� ������ Decoder.cs. ��� ����� �������� �������.