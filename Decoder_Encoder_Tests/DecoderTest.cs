using Decoder_Encoder;
using NUnit.Framework;

namespace Tests
{
	public class DecoderTest
	{
		[Test]
		public void DecodeByOneKeyTestWithSomeKey()
		{
			string text = "��� ����,������.�����:������!����?���� abc...xyz";
			int key = 5;
			bool direction = true;
			string expected = "�� �����,������.�����:������!����?���� abc...xyz";

			Decoder decoder = new Decoder();
			string actual = decoder.DecodeByOneKey(text, key, direction);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void DecodeByOneKeyTestWithZeroKey()
		{
			string text = "��� ����,������.�����:������!����?���� abc...xyz";
			int key = 0;
			bool direction = true;
			string expected = "��� ����,������.�����:������!����?���� abc...xyz";

			Decoder decoder = new Decoder();
			string actual = decoder.DecodeByOneKey(text, key, direction);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void DecodeByOneKeyTestWith33Key()
		{
			string text = "��� ����,������.�����:������!����?���� abc...xyz";
			int key = 33;
			bool direction = true;
			string expected = "��� ����,������.�����:������!����?���� abc...xyz";

			Decoder decoder = new Decoder();
			string actual = decoder.DecodeByOneKey(text, key, direction);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void DecodeByOneKeyTestWithFalseDeirection()
		{
			string text = "��� ����,������.�����:������!����?���� abc...xyz";
			int key = 1;
			bool direction = false;
			string expected = "��� ����,������.�����:������!����?���� abc...xyz";

			Decoder decoder = new Decoder();
			string actual = decoder.DecodeByOneKey(text, key, direction);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void DecodeByOneKeyTestWithEmptyText()
		{
			string text = "";
			int key = 13;
			bool direction = true;
			string expected = "";

			Decoder decoder = new Decoder();
			string actual = decoder.DecodeByOneKey(text, key, direction);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void DecodeByAllKeysTest()
		{
			string text = "�";
			string expected = "����: 1\n\n�\n\n\n����: 2\n\n�\n\n\n����: 3\n\n�\n\n\n����: 4\n\n�\n\n\n����: 5\n\n�" +
				"\n\n\n����: 6\n\n�\n\n\n����: 7\n\n�\n\n\n����: 8\n\n�\n\n\n����: 9\n\n�\n\n\n����: 10\n\n�\n\n\n" +
				"����: 11\n\n�\n\n\n����: 12\n\n�\n\n\n����: 13\n\n�\n\n\n����: 14\n\n�\n\n\n����: 15\n\n�\n\n\n" +
				"����: 16\n\n�\n\n\n����: 17\n\n�\n\n\n����: 18\n\n�\n\n\n����: 19\n\n�\n\n\n����: 20\n\n�\n\n\n" +
				"����: 21\n\n�\n\n\n����: 22\n\n�\n\n\n����: 23\n\n�\n\n\n����: 24\n\n�\n\n\n����: 25\n\n�\n\n\n" +
				"����: 26\n\n�\n\n\n����: 27\n\n�\n\n\n����: 28\n\n�\n\n\n����: 29\n\n�\n\n\n����: 30\n\n�\n\n\n" +
				"����: 31\n\n�\n\n\n����: 32\n\n�\n\n\n����: 33\n\n�";

			Decoder decoder = new Decoder();
			string actual = decoder.DecodeByAllKeys(text);
			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void FrequencyAnalysisTest()
		{
			string text = "������";
			string expected = "������������ ����: -1\n\n������";

			Decoder decoder = new Decoder();
			string actual = decoder.FrequencyAnalysis(text);
			Assert.AreEqual(expected, actual);
		}
	}
}